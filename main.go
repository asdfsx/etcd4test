package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"sync"

	"github.com/coreos/etcd/clientv3"
)

func etcdtest(wg *sync.WaitGroup) {
	defer wg.Done()
	var cli *clientv3.Client

	for {
		if cli == nil {
			tmp, err := clientv3.New(clientv3.Config{
				Endpoints: []string{"127.0.0.1:2379"},
				// DialTimeout: 50 * time.Microsecond, // 时间不能太短，不然会直接失败
			})
			if err != nil {
				fmt.Println("---", err)
				time.Sleep(5 * time.Second)
				continue
			} else {
				cli = tmp
			}
		}

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		resp, err := cli.Get(ctx, "foo")
		cancel()

		switch err {
		case context.DeadlineExceeded:
			fmt.Println("++++1", err)
		}

		switch resp {
		case nil:
			resp, err := cli.Put(context.TODO(), "foo", "bar")
			if err != nil {
				fmt.Println("++++2", err)
			}
			fmt.Println("++++3", resp)
		default:
			fmt.Println("++++4", resp)
		}

		time.Sleep(5 * time.Second)
	}
}

func etcdtest2(wg *sync.WaitGroup) {
	defer wg.Done()
	var cli *clientv3.Client

	cli, err := clientv3.New(clientv3.Config{
		Endpoints: []string{"127.0.0.1:2379"},
		// DialTimeout: dialTimeout,
	})
	if err != nil {
		log.Fatal("====", err)
	}
	defer cli.Close()

	_, err = cli.Put(context.TODO(), "foo", "bar")
	if err != nil {
		log.Fatal("====", err)
	}

	resp, err := cli.Get(context.TODO(), "foo")

	if err != nil {
		log.Fatal(err)
	}
	for _, ev := range resp.Kvs {
		fmt.Printf("%s : %s\n", ev.Key, ev.Value)
	}
}

func etcdtest3(wg *sync.WaitGroup) {
	defer wg.Done()
	var cli *clientv3.Client
	var leaseid clientv3.LeaseID

	for {
		if cli == nil {
			tmp, err := clientv3.New(clientv3.Config{
				Endpoints:   []string{"127.0.0.1:2379"},
				DialTimeout: 5 * time.Second, // 时间不能太短，不然会直接失败
			})
			if err != nil {
				fmt.Println("---", err)
				time.Sleep(5 * time.Second)
				continue
			} else {
				_, err = cli.Put(context.TODO(), "foo", "bar")
				if err != nil {
					log.Fatal("====", err)
				}

				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				resp, err := cli.Grant(ctx, 5)
				if err != nil {
					fmt.Println("=-=-=-=", err)
				}
				cancel()

				leaseid = resp.ID
				cli = tmp
			}
		}

		_, err := cli.Put(context.TODO(), "foo", "bar", clientv3.WithLease(leaseid))
		if err != nil {
			fmt.Println(err)
		}

		time.Sleep(5 * time.Second)
	}
}

func main() {

	wg := &sync.WaitGroup{}

	wg.Add(1)

	go etcdtest3(wg)

	wg.Wait()
}
