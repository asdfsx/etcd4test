# swarm下启动etcd
[etcd 使用入门](http://cizixs.com/2016/08/02/intro-to-etcd)

etcdv3的配置要注意：
* v3的连接配置要用 --listen-client-urls 参数，对应环境变量 ETCD_LISTEN_CLIENT_URLS
* 默认的连接配置是：localhost:2379，所以容器意外是连不了的。需要改为 0.0.0.0:2379
* 当使用 --listen-client-urls 后，要配置 --advertise-client-urls
* --advertise-client-urls 的作用：对外公告的该节点客户端监听地址，这个值会告诉集群中其他节点

```
docker swarm init

docker network  create \
    --driver overlay \
    --subnet 10.0.9.0/24 \
    --opt encrypted \
    my-network

docker service create \
    --name etcd \
    --network my-network \
    --publish 2379:2379 \
    --publish 2380:2380 \
    --env ETCD_LISTEN_CLIENT_URLS="http://0.0.0.0:2379" \
    --env ETCD_ADVERTISE_CLIENT_URLS="http://etcd:2379" \
    quay.io/coreos/etcd:v3.1.1
```
